export class Pipeline {

    constructor(config) {
        this.context = config;
    }

    addHandler(handlerObject) {
        let returnCanHandler = handlerObject.canHandler(this.context);

        switch (typeof returnCanHandler) {
            case "boolean":
                return returnCanHandler === true
                    ? handlerObject.handlerRun()
                    : handlerObject.willNotRun();
            case "object":
                return returnCanHandler.then((value) => handlerObject.canHandlerPromise(value))
                    .then((bool) => bool === true
                        ? handlerObject.handlerRun()
                        : handlerObject.willNotRun());
            default:
                fail("Erro ao definir retorno do canHandler");
        }
    }

}