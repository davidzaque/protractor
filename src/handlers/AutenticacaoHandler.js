import {Autenticacao} from "../../pageObjects/Autenticacao.po";

const autenticacao = new Autenticacao();

export class AutenticacaoHandler {

    constructor(config) {
        this.config = config;
    }

    canHandler(context) {
        return browser.getCurrentUrl();
    }

    canHandlerPromise(url) {
        let regex = new RegExp(/http:\/\/a.testaddressbook.com\/sign_in/);

        return !!url.match(regex) ? true : this.willNotRun("A Pré condição da URL não foi atendida!");
    }

    willNotRun(error) {
        return error;
    }

    handlerRun() {
        return autenticacao[this.config.method](this.config);
    }

}