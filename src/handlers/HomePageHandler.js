import {HomePage} from "../../pageObjects/HomePage.po";

const homePage = new HomePage();

export class HomePageHandler {

    constructor(config) {
        this.config = config;
    }

    canHandler(context) {
        return true;
    }

    handlerRun(context) {
        return homePage[this.config.method](this.config);
    }

}