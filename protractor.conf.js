'use strict';

require('babel-core/register.js');

const myReporter = require('./report/MyReporter');

module.exports.config = {
    allScriptsTimeout: 30000,
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            'args': ['--test-type']
        }
    },
    specs: ['Specs/*.spec.js'],
    onPrepare() {
        jasmine.getEnv().addReporter(myReporter);
        browser.driver.manage().window().maximize();
        browser.waitForAngularEnabled(false);
    },
    jasmineNodeOpts: {
        showColors: true,
        silent: true,
        defaultTimeoutInterval: 600000,
        isVerbose: false,
        includeStackTrace: false,
        print: () => {
        }
    },
    params: {
        cenario: {
            ambiente: 'demo'
        }
    }
};
