'use strict';

const pEC = protractor.ExpectedConditions, bEC = browser.ExpectedConditions;

export class ExpectingHelper {

    waitForElementPresent(el) {
        return browser.wait(pEC.presenceOf(el), 60000, 'Element taking too long to appear in the DOM');
    }

    waitForElementClickable(el) {
        return browser.wait(pEC.elementToBeClickable(el), 5000, 'Element taking too long to be clickable');
    }

}