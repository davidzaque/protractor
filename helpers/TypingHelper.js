'use strict';

export class TypingHelper {

    trySendKeys(el, value) {
        return (value === undefined || value === null)
            ? Promise.resolve()
            : el.sendKeys(value)
                .then(() => Promise.resolve())
                .catch((e) => Promise.reject(e));
    }

}