const fs = require('fs');
const papa = require('papaparse');
const jsonFile = require('jsonfile');

export class ScenarioHelper {

    constructor() {
        this.csvUsuarios = fs.readFileSync("/Invillia/protractor/data/usuarios.csv", "utf8");
        this.arrayJsonUsers = jsonFile.readFileSync("/Invillia/protractor/data/usuarios.json", "utf8")
    }

    getCurrentHourMinutesSeconds() {
        let currentDate = new Date();
        let hours = currentDate.getHours();
        let minutes = currentDate.getMinutes();
        let seconds = currentDate.getSeconds();

        if (hours < 10) {
            hours = '0' + hours;
        }

        if (minutes < 10) {
            minutes = '0' + minutes;
        }

        if (seconds < 10) {
            seconds = '0' + seconds;
        }

        return (hours + ':' + minutes + ':' + seconds);
    }

    getSenhaUsuarioCSV(usuario) {
        return new Promise((resolve) => {
            papa.parse(this.csvUsuarios, {
                complete: (result) => {
                    let arrayCabecalho = result.data[0],
                        indexUsuario = arrayCabecalho.indexOf('usuario'),
                        indexSenha = arrayCabecalho.indexOf('senha');

                    for (let i = 1; i < result.data.length - 1; i++) {
                        let userData = result.data[i][indexUsuario].toString(),
                            senhaData = result.data[i][indexSenha].toString();

                        if (usuario === userData)
                            return resolve(senhaData);
                    }

                }
            })
        })
    }

    getUsuarioSenhaByIndexCSV(index) {
        return new Promise((resolve, reject) => {
            papa.parse(this.csvUsuarios, {
                complete: (result) => {
                    let arrayCabecalho = result.data[0],
                        indexUsuario = arrayCabecalho.indexOf('usuario'),
                        indexSenha = arrayCabecalho.indexOf('senha');

                    if (index > result.data.length)
                        return reject("Index maior qua a base de usuarios");
                    else
                        return resolve({
                            usuario: result.data[index][indexUsuario].toString(),
                            senha: result.data[index][indexSenha].toString()
                        });
                }
            })
        })
    }

    retiraCaracterEspecial(str) {
        return str.replace(";","");
    }

}
