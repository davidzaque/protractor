import {ScenarioHelper} from "../helpers/ScenarioHelper";

const clc = require("cli-color");
const errorColor = clc.red.bold;
const passedColor = clc.green;
const warnColor = clc.yellow;
const noticeColor = clc.blue;
const scenarioHelper = new ScenarioHelper();
const ok = '\u2713';
const nOk = 'x ';
let inicio, inicioSuite, fim, fimSuite, duracao, duracaoSuite = null;

const MyReporter = {
    suiteStarted: (result) => {
        inicioSuite = Date.now();
        console.log(warnColor('Suite: ' + result.fullName + ' started'));
    },
    specStarted: (result) => {
        inicio = Date.now();
    },
    specDone: (result) => {
        let status = result.status, casoTeste = result.description,
            suite = result.fullName.replace(result.description, ''),
            now = scenarioHelper.getCurrentHourMinutesSeconds();

        fim = Date.now();
        duracao = ((fim - inicio) * 0.001).toFixed(2);

        switch (status.toString()) {
            case 'failed':
                if (result.failedExpectations.length > 0) {
                    for (let i = 0; i < result.failedExpectations.length; i++) {
                        console.log('\t' + noticeColor(now) + ' ' + errorColor(nOk + casoTeste) + ' (' + duracao + ' s)');
                        console.log('\t\t' + clc.red(result.failedExpectations[i].message));
                    }
                }
                break;
            case 'passed':
                console.log('\t' + noticeColor(now) + ' ' + passedColor(ok + casoTeste) + ' (' + duracao + ' s)');
                break;
            case 'pending':
                console.log('\t' + noticeColor(now) + ' ' + warnColor('skipped: ' + casoTeste) + ' (' + duracao + ' s)');
                break;
            case 'disabled':
                console.log('\t' + noticeColor(now) + ' ' + warnColor('disabled: ' + casoTeste) + ' (' + duracao + ' s)');
                break;
            default:
                console.log(errorColor("Erro ao definir o status do teste!"));
                break;
        }
    },
    suiteDone: (result) => {
        fimSuite = Date.now();
        duracaoSuite = ((fimSuite - inicioSuite) * 0.001).toFixed(2);
        console.log(warnColor('Suite: ' + result.description + ' ' + result.status) + ' (' + duracaoSuite + ' s)');
    }
};

module.exports = MyReporter;
