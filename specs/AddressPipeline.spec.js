'use strict';

import {HomePage} from "../pageObjects/HomePage.po";
import {HomePageHandler} from "../src/handlers/HomePageHandler";
import {Pipeline} from "../src/Pipeline";
import {AutenticacaoHandler} from "../src/handlers/AutenticacaoHandler";

const homePage = new HomePage();
let pipeline = new Pipeline();

describe('Addresses test pages', () => {

    beforeEach(() => {
        pipeline.addHandler(new HomePageHandler({
            method: 'visitConfig',
            afterAll: () => {
                homePage.clickMenuSignIn();
            }
        }));
    });

    it('Tentar autenticar sem preencher os campos', () => {
        pipeline.addHandler(new AutenticacaoHandler({
            method: "fillForm",
            willNotRun: (error) => {
                fail(error);
            },
            afterAll: () => {
                expect(homePage.getErrorMessageForm()).toBe("Bad email or password.");
            }
        }))
    });

    it('Tentar autenticar preenchendo apenas o usuário', () => {
        pipeline.addHandler(new AutenticacaoHandler({
            method: "fillForm",
            userEmail: "teste@hotmail.com",
            willNotRun: (error) => {
                fail(error);
            },
            afterAll: () => {
                expect(homePage.getErrorMessageForm()).toBe("Bad email or password.");
            }
        }))
    });

    it('Tentar autenticar preenchendo apenas a senha', () => {
        pipeline.addHandler(new AutenticacaoHandler({
            method: "fillForm",
            userEmail: "teste@hotmail.com",
            willNotRun: (error) => {
                fail(error);
            },
            afterAll: () => {
                expect(homePage.getErrorMessageForm()).toBe("Bad email or password.");
            }
        }))
    });

    it('Realizar a autenticação dos campos com dados válidos', () => {
        pipeline.addHandler(new AutenticacaoHandler({
            method: "fillForm",
            userEmail: "teste@hotmail.com",
            pass: "teste",
            willNotRun: (error) => {
                fail(error);
            },
            afterAll: () => {
                expect(homePage.getHomeMessage()).toBe("Welcome to Address Book");
                expect(homePage.signOut.isPresent()).toBe(true);
            }
        }))
    });

});