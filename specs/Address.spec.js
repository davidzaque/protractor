'use strict';

import {Autenticacao} from "../pageObjects/Autenticacao.po";
import {HomePage} from "../pageObjects/HomePage.po";

const autenticacao = new Autenticacao();
const homePage = new HomePage();

describe('Addresses test pages', () => {

    it('Tentar autenticar sem preencher os campos', () => {
        return homePage.visit()
            .then(() => homePage.clickMenuSignIn())
            .then(() => autenticacao.fillForm())
            .then(() => expect(homePage.signOut.isPresent()).toBe(true))
    });

    it('Tentar autenticar preenchendo apenas o usuário', () => {
        return homePage.visit()
            .then(() => homePage.clickMenuSignIn())
            .then(() => autenticacao.fillForm("teste@hotmail.com"))
            .then(() => expect(homePage.signOut.isPresent()).toBe(true))
    });

    xit('Tentar autenticar preenchendo apenas a senha', () => {

    });

    xit('Realizar a autenticação dos campos com dados válidos', () => {

    });

});