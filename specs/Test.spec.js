import {ScenarioHelper} from "../helpers/ScenarioHelper";

const scenarioHelper = new ScenarioHelper();

describe('Test data files', () => {

    it('Teste com CSV', () => {
        scenarioHelper.getUsuarioSenhaByIndexCSV(1)
            .then((objUserPass) => {
                return expect(objUserPass.usuario).toBe("teste@gmail.com")
            });
    });

    scenarioHelper.arrayJsonUsers.forEach((p, index) => {
        it("[CASO DE TESTE DINÂMICO JSON]: Validando senha do Usuario: " + p.user, () => {
            expect(p.pass).toBe("123");
        })
    });

});