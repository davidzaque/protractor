import {ScenarioHelper} from "../helpers/ScenarioHelper";

const scenarioHelper = new ScenarioHelper();

describe('Describe', () => {

    it('Teste com CSV', () => {
        scenarioHelper.getUsuarioSenhaByIndex(1)
            .then((objUserPass) => {
                return expect(objUserPass.usuario).toBe("teste@hotmail.com")
            });
    });

    scenarioHelper.arrayJsonUsers.forEach((p, index) => {
        it("[CASO DE TESTE DINÂMICO]: Validando senha do Usuario: " + p.user, () => {
            expect(p.pass).toBe("123");
        })
    });

});