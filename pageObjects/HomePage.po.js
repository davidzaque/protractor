import {ExpectingHelper} from "../helpers/ExpectingHelper";

const expectingHelper = new ExpectingHelper();

export class HomePage {

    constructor() {
        this.signIn = element(by.css('a[data-test="sign-in"]'));
        this.welcomeMessage = element(by.css('div[class="text-center"]>h1'));
        this.errorMessageForm = element(by.css('div[data-test="notice"]'));
        this.signOut = element(by.css('a[data-test="sign-out"]'));
    }

    visitConfig(config) {
        return browser.get("http://a.testaddressbook.com/")
            .then(() => config.afterAll())
            .catch((e) => {
                let message = "Erro no método visit: " + e;

                return Promise.reject(message);
            })
    }

    clickMenuSignIn() {
        return expectingHelper.waitForElementClickable(this.signIn)
            .then(() => this.signIn.click())
            .then(() => Promise.resolve())
            .catch((e) => {
                let message = "Erro no método clickMenuSignIn: " + e;

                return Promise.reject(message);
            })
    }

    getErrorMessageForm() {
        return this.errorMessageForm.getText()
            .then((text) => Promise.resolve(text))
            .catch((e) => {
                let message = "Erro no método getErrorMessageForm: " + e;

                return Promise.reject(message);
            })
    }

    getHomeMessage() {
        return this.welcomeMessage.getText()
            .then((text) => Promise.resolve(text))
            .catch((e) => {
                let message = "Erro no método getHomeMessage: " + e;

                return Promise.reject(message);
            })
    }

}