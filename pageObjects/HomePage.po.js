import {browser} from "protractor";
import {ExpectingHelper} from "../helpers/ExpectingHelper";

const expectingHelper = new ExpectingHelper();

export class HomePage {

    constructor() {
        this.home = element(by.css('a[data-test="home"]'));
        this.signIn = element(by.css('a[data-test="sign-in"]'));
        this.homeTitle = element(by.css('div[class="text-center"]>h1'));
        this.signOut = element(by.css('a[data-test="sign-out"]'));
    }

    visit() {
        return browser.get("http://a.testaddressbook.com/")
            .then(() => Promise.resolve())
            .catch((e) => {
                let message = "Erro no método visit: " + e;

                return Promise.reject(message);
            })
    }

    clickMenuHome() {
        return this.home.click()
            .then(() => Promise.resolve())
            .catch((e) => {
                let message = "Erro no método clickMenuHome: " + e;

                return Promise.reject(message);
            })
    }

    clickMenuSignIn() {
        return expectingHelper.waitForElementClickable(this.signIn)
            .then(() => this.signIn.click())
            .then(() => Promise.resolve())
            .catch((e) => {
                let message = "Erro no método clickMenuSignIn: " + e;

                return Promise.reject(message);
            })
    }

    clickMenuSignOut() {
        return this.signOut.click()
            .then(() => Promise.resolve())
            .catch((e) => {
                let message = "Erro no método clickMenuSignOut: " + e;

                return Promise.reject(message);
            })
    }

    getHomeTitle() {
        return this.homeTitle.getText()
            .then((text) => Promise.resolve(text))
            .catch((e) => {
                let message = "Erro no método getHomeTitle: " + e;

                return Promise.reject(message);
            })
    }

}