'use strict';

import {ExpectingHelper} from "../helpers/ExpectingHelper";
import {ScenarioHelper} from "../helpers/ScenarioHelper";
import {TypingHelper} from "../helpers/TypingHelper";

const expectingHelper = new ExpectingHelper();
const scenarioHelper = new ScenarioHelper();
const typingHelper = new TypingHelper();

export class Autenticacao {

    constructor() {
        this.submitForm = element(by.css('input[type="Submit"]'));
        this.email = element(by.id('session_email'));
        this.password = element(by.id('session_password'));
        this.autenticacaoTitle = element(by.css('#clearance>h2'));
        this.notice = element(by.css('div[data-test="notice"]'));
    }

    fillEmail(email) {
        return expectingHelper.waitForElementClickable(this.email)
            .then(() => typingHelper.trySendKeys(this.email, email))
            .then(() => Promise.resolve())
            .catch((e) => {
                let message = "Erro no método fillEmail: \n\t\t\t" + e;

                return Promise.reject(message);
            })
    }

    fillPassword(pass) {
        return expectingHelper.waitForElementClickable(this.password)
            .then(() => typingHelper.trySendKeys(this.password, pass))
            .then(() => Promise.resolve())
            .catch((e) => {
                let message = "Erro no método fillPassword: \n\t\t\t" + e;

                return Promise.reject(message);
            })
    }

    getAutenticacaoTitle() {
        return this.autenticacaoTitle.getText()
            .then((text) => Promise.resolve(text))
            .catch((e) => {
                let message = "Erro no método getAutenticacaoTitle: \n\t\t\t" + e;

                return Promise.reject(message);
            })
    }

    getNotice() {
        return this.notice.getText()
            .then((text) => Promise.resolve(text))
            .catch((e) => {
                let message = "Erro no método getNotice: \n\t\t\t" + e;

                return Promise.reject(message);
            })
    }

    fillForm(email, pass) {
        return this.fillEmail(email)
            .then(() => this.fillPassword(pass))
            .then(() => this.submitForm.click())
            .catch((e) => {
                let message = "Erro no método fillForm: \n\t\t\t" + e;

                return Promise.reject(scenarioHelper.retiraCaracterEspecial(message));
            })
    }

}