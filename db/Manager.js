const {Pool} = require('pg');

export class Manager {

    pool(database) {
        let connectionString;

        switch (database) {
            case 'prod':
                connectionString = 'postgresql://admin:admin@127.0.0.1:9999/prod';
                break;
            case 'homologacao':
                connectionString = 'postgresql://admin:admin@127.0.0.1:9999/homo';
                break;
            case 'demo':
                connectionString = 'postgresql://admin:admin@127.0.0.1:9999/demo';
                break;
            default:
                connectionString = 'postgresql://admin:admin@127.0.0.1:9999/deve';
                break;
        }

        return new Pool({connectionString: connectionString});
    }

    findOne(ambienteDatabase, sql) {
        let conexao = this.pool(ambienteDatabase);

        return conexao.query(sql)
            .then(res => {
                conexao.end();
                return Promise.resolve(res.rows[0])
            })
            .catch((e) => console.error('Error executing query', e.stack))
    }

    findAll(ambienteDatabase, sql) {
        let conexao = this.pool(ambienteDatabase);

        return conexao.query(sql)
            .then(res => {
                conexao.end();
                return Promise.resolve(res.rows);
            })
            .catch((e) => console.error('Error executing query', e.stack))
    }

}
